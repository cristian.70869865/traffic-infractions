import express  from 'express'
import Application from './app';
import dataBase from './config/database';
const application = new Application(express).getApplication();

dataBase.connect();
application.listen(process.env.PORT || 500, () => {
  console.log('API Rest corriendo.');
})